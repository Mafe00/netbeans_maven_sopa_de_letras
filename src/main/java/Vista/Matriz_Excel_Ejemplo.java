/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.SopaDeLetras;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;
import java.util.Scanner;


/**
 *
 * @author madarme
 */
public class Matriz_Excel_Ejemplo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("Palabra a encontrar: ");
        String palabra1 = sc.nextLine();
        SopaDeLetras s = new SopaDeLetras(palabra1);
        try{
        s.leerExcel();
        System.out.println(s.izquierdaDerecha());
        System.out.println(s.derechaIzquierda());
        System.out.println(s.abajoArriba());
        System.out.println(s.arribaAbajo());
        }catch(Exception e){
            System.err.println("Error, matriz dispersa.");
        }
        }
    }
