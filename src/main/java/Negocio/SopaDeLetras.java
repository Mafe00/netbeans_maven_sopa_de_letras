package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.util.ArrayList;
import java.lang.Math.*;

/**
 * Write a description of class SopaLetra here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras implements OperacioMatriz{
    // La matriz con las letras de la sopa

     private char sopa[][];
     protected ArrayList <String> arreglo; 
     private String palabra = "";

    /**
     * Constructor for objects of class SopaLetras
     */
    
    public SopaDeLetras(String palabra){
        this.palabra =  palabra.toUpperCase();
    }

    /*public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("No se puede crear la sopa");
        }

        String palabras2[] = palabras.split(",");
        //Crear la cantidad de filas:
        this.sopa = new char[palabras2.length][];
        //recorrer cada elemento de palabras2 y pasarlo a su correspondiente fila en sopa:
        int i = 0;
        for (String palabraX : palabras2) {
            this.sopa[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopa[i]);
            i++;
        }

    }*/

    private void pasar(String x, char vector[]) {
        for (int j = 0; j < x.length(); j++) {
            vector[j] = x.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopa.length; i++) {
            for (int j = 0; j < this.sopa[i].length; j++) {
                msg += this.sopa[i][j] + " " + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char vector[] : this.sopa) {
            for (char dato : vector) {
                msg += dato + "" + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    /**
     * Retorna la letra que más se repite
     */
    public char get_MasSeRepite()
    { 
        char letra = ' ';
        int cont = 0;
        int letraRepetida = 0;
        for(int i = 0;i < sopa.length; i++){
            for(int j = 0;j < sopa[i].length;j++){
                char letra2 = sopa[i][j];
                for(int k = 0 ; k < sopa.length; k++){
                    for(int l = 0; l < sopa[k].length; l++){
                        if(letra2 == sopa[k][l])
                            cont++;
                    }  
                }
                if(cont>letraRepetida){
                    letraRepetida = cont;
                    letra = letra2;
                }
                cont = 0;
            }
        }
        return letra;   
    }   

    public boolean esRectangular(){
        for(int i = 0; i < sopa.length;i++){
            if(!(sopa.length != sopa[i].length) || !(sopa[i].length != sopa.length)){
                return false;
            }
        }
        return true;
    }

    public boolean esDispersa() {
        int cont = 1;
        for(int i = 0; i < sopa.length; i++) {
            if (sopa[0].length == sopa[cont].length){
                cont++;
                return false;                
            }
        }
        return true;
    }

    public boolean esCuadrada(){
        for(int i = 0; i < sopa.length; i++){
            if(sopa [i].length != sopa.length){
                return false;
            }
        }
        return true;
    }

    public char[] getDiagonalInferior() throws Exception {
        //si y solo si es cuadrada , si no, lanza excepcion
        if(esCuadrada()){
            char [] arreglo = new char[sopa.length];
            for(int i = 0;i < sopa.length;i++){
                for(int j = 0;j < sopa[i].length; j++){
                    if(i+j == sopa.length-1){
                        arreglo[i] = sopa[i][j];
                    }
                }
            }
            return arreglo;
        }else{
            throw new Exception("No es cuadrada.");
        }      
    }

    //Start GetterSetterExtension Code
    /**Getter method sopa*/
    public char[][] getSopa(){
        return this.sopa;
    }//end method getSopa

    /**Setter method sopa*/
    public void setSopa(char[][] sopa){
        this.sopa = sopa;
    }//end method setSopa

    //End GetterSetterExtension Code
    //!

    public int getCantCol(int fila){
        if(sopa == null || fila>=sopa.length || fila <0){
            return -1;
        }else{
            return sopa[fila].length;
        }     
    }

    public int filaMasDatos(){
        int cont =0 ;
        int valor =0;
        if(sopa == null){
            return -1;
        }else{
            for(int i  = 0;i < sopa.length;i++){
                for(int j = 1; j < sopa.length; j++ ){
                    cont = sopa[i].length;

                    if(cont < sopa[j].length){
                        valor = j;
                    }
                }
                return valor;
            }

        }
        return -1;
    }
    
    public void leerExcel() throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("data3.xls"));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        int cantCol = 0;
        String[][]d = new String[canFilas][];
        
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            cantCol=filas.getLastCellNum();
            d [i] = new String[cantCol];
            
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            d[i][j]=filas.getCell(j).getStringCellValue();
        }
       }
        sopa = new char[canFilas][cantCol];
        if(esCuadrada() || esRectangular()){
        
        for(int k = 0; k < d.length;k++){
            for(int o = 0; o < d[k].length;o++){
                sopa[k][o]= d[k][o].charAt(0);
            }
        }
        }else{
            
            throw new Exception("Error, la matriz es dispersa.");
        }
    }
    
    public ArrayList<String> izquierdaDerecha(){
        arreglo = new ArrayList(1);
        String palabra2 = "";
        byte k = 0,num = 0;
        for(int i = 0; i < sopa.length;i++){
            palabra2 = "";
            k = 0;
            for(int j = 0; j < sopa[i].length;j++){
                if(sopa[i][j] == this.palabra.charAt(k)){
                    k++;
                    if(k == this.palabra.length()){k = 0;}
                    palabra2 += sopa[i][j];
                    if(this.palabra.equals(palabra2)){
                        num++;
                        palabra2 = "";
                        k = 0;                      
                        arreglo.add("Fila: "+i+" - Entre Columna: "+(j-this.palabra.length()+1)+" y " +j);
                    }
                }else{
                    palabra2 = "";
                    k = 0;
                }
            }
        }
        arreglo.add(num,"La palabra se encontró "+num+" veces.");
        return arreglo;
    }
    
    public ArrayList<String> derechaIzquierda(){
        arreglo = new ArrayList(1);
        String palabra2 = "";
        byte k = 0,num = 0;
        for(int i = 0; i < sopa.length;i++){
            palabra2 = "";
            k = 0;
            for(int j = sopa[i].length-1; j > -1;j--){
                if(sopa[i][j] ==  this.palabra.charAt(k)){
                    k++;
                    if(k == this.palabra.length()){k = 0;}
                    palabra2 += sopa[i][j];
                    if(this.palabra.equals(palabra2)){
                        num++;
                        palabra2 = "";
                        k = 0;
                        arreglo.add("Fila: "+i+" - Entre Columna: "+j+" y " +Math.abs(j-this.palabra.length()+1));
                    }
                }else{
                    palabra2 = "";
                    k = 0;
                }
            }
        }                    
        arreglo.add(num,"La palabra se encontró "+num+" veces.");
        return arreglo;
    }

    public ArrayList<String> arribaAbajo(){
        arreglo = new ArrayList(1);
        String palabra2 = "";
        byte k = 0,num = 0;
        for(int i = 0; i < sopa[i].length;i++){
            palabra2 = "";
            k = 0;
            for(int j = 0; j < sopa.length; j++){
                if(sopa[j][i] ==  this.palabra.charAt(k)){
                    k++;
                    if(k == this.palabra.length()){k = 0;}
                    palabra2 += sopa[j][i];
                    if(this.palabra.equals(palabra2)){
                        num++;
                        palabra2 = "";
                        k = 0;
                        arreglo.add("Columna: "+i+" - Entre Filas: "+j+" y " +Math.abs(j-this.palabra.length()+1));
                    }
                }else{
                    palabra2 = "";
                    k = 0;
                }
            }
        }
        arreglo.add(num,"La palabra se encontró "+num+" veces.");
        return arreglo;
    }

    public ArrayList<String> abajoArriba(){
        arreglo = new ArrayList(1);
        String palabra2 = "";
        byte k = 0,num = 0;
        for(int i = 0; i < sopa[i].length;i++){
            palabra2 = "";
            k = 0;
            for(int j = sopa.length-1;j > -1;j--){
                if(sopa[j][i] == this.palabra.charAt(k)){
                    k++;
                    if(k == this.palabra.length()){k = 0;}
                    palabra2 += sopa[j][i];
                    if(this.palabra.equals(palabra2)){
                        num++;
                        palabra2 = "";
                        k = 0;
                        arreglo.add("Columna: "+i+" - Entre Filas: "+Math.abs(j-this.palabra.length()+1)+" y " +j);
                    }
                }else{
                    palabra2 = "";
                    k = 0;
                }
            }
        }
        arreglo.add(num,"La palabra se encontró "+num+" veces.");
        return arreglo;
    }
}