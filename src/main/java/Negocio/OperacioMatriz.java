package Negocio;
import java.util.ArrayList;

/**
 * Write a description of interface OperacioMatriz here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public interface OperacioMatriz 
{
    public int getCantCol(int fila);
    public int filaMasDatos();
    
    public ArrayList<String> izquierdaDerecha();
    public ArrayList derechaIzquierda();
    public ArrayList arribaAbajo();
    public ArrayList abajoArriba();
}
